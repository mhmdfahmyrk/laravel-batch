<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Sign Up Form</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/kirim" method="post">
      @csrf
      <label for="First Name">First Name:</label>
      <br />
      <br />
      <input type="text" name="First Name" />
      <br />
      <br />
      <label for="Last Name">Last Name:</label>
      <br />
      <br />
      <input type="text" name="Last Name" />
      <br />
      <p>Gender:</p>
      <input type="radio" id="Male" name="gender" /><label for="Male"
        >Male</label
      >
      <br />
      <input type="radio" id="Female" name="gender" /><label for="Female"
        >Female</label
      >
      <br />
      <input type="radio" id="Other" name="gender" /><label for="Other"
        >Other</label
      >
      <br />
      <p>Nationality:</p>
      <select name="Nationality" id="Nationality">
        <option value="">Indonesian</option>
        <option value="">Singaporean</option>
        <option value="">Malaysian</option>
        <option value="">Australian</option>
      </select>
      <br />
      <p>Language Spoken:</p>
      <input type="checkbox" id="Bahasa Indonesia" /><label
        for="Bahasa Indonesia"
        >Bahasa Indonesia</label
      >
      <br />
      <input type="checkbox" id="English" /><label for="English">English</label>
      <br />
      <input type="checkbox" id="Other" /><label for="Other">Other</label>
      <br />
      <p>Bio:</p>
      <textarea name="Bio" id="Bio" cols="30" rows="10"></textarea>
      <br />
      <button type="submit">Sign Up</button>
    </form>
  </body>
</html>
