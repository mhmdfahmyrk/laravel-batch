<?php
// require('animal.php');
require('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");

echo "Nama Hewan : " . $sheep->name . "<br>";
echo "Jumlah Kaki : " . $sheep->legs . "<br>";
echo "Cold Blooded? : " . $sheep->cold_blooded . "<br>" . "<br>";

$frog = new Frog("buduk");

echo "Nama Hewan : $frog->name <br>";
echo "Jumlah Kaki : $frog->legs <br>";
echo "Cold Blooded : $frog->cold_blooded <br>";
$frog->jump();
echo "<br> <br>";

$ape = new Ape("kera sakti");

echo "Nama Hewan : $ape->name <br>";
echo "Jumlah Kaki : $ape->legs <br>";
echo "Cold Blooded? : $ape->cold_blooded <br>";
$ape->yell();